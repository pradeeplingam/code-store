provider "aws" {
region = "ap-south-1"
access_key = ""
secret_key = ""
}

resource "aws_ecs_task_definition" "service" {
family = var.family
requires_compatibilities = ["FARGATE"]
network_mode= "awsvpc"
cpu= var.cpu
memory    = var.memory
container_definitions = jsonencode([
{
name      = var.container_name
image     = var.image
essential = true
portMappings = [
{
containerPort = 80
}
]
}
])
}

data "aws_vpc" "default" {
default = true
}

data "aws_subnet_ids" "default" {
vpc_id = "${data.aws_vpc.default.id}"
}

resource "aws_security_group" "lb" {
name= "lb_sg"
description = "allow inbound access from the ALB only"

ingress {
protocol= "tcp"
from_port= 80
to_port= 80
cidr_blocks     = ["0.0.0.0/0"]
}

egress {
protocol    = "-1"
from_port   = 0
to_port     = 0
cidr_blocks = ["0.0.0.0/0"]
}
}

resource "aws_security_group" "ecs_tasks" {
name= "ecs_tasks"
description = "allow inbound access from the ALB only"

ingress {
protocol= "tcp"
from_port= 80
to_port= 80
cidr_blocks     = ["0.0.0.0/0"]
security_groups = [aws_security_group.lb.id]
}

egress {
protocol    = "-1"
from_port   = 0
to_port     = 0
cidr_blocks = ["0.0.0.0/0"]
}
}

resource "aws_lb" "app" {
name = "alb"
subnets = data.aws_subnet_ids.default.ids
load_balancer_type = "application"
security_groups = [aws_security_group.lb.id]

tags = {
Environment = "app"
Application = "nginx"
}
}

resource "aws_lb_listener" "https" {
load_balancer_arn = aws_lb.app.arn
port = 80
protocol = "HTTP"

default_action {
type = "forward"
target_group_arn = aws_lb_target_group.app.arn
}
}

resource "aws_lb_target_group" "app" {
name = "nginx-alb-tg"
port = 80
protocol = "HTTP"
vpc_id = data.aws_vpc.default.id
target_type = "ip"

health_check {
healthy_threshold = "3"
interval = "60"
protocol = "HTTP"
matcher = "200-299"
timeout = "20"
path = "/"
unhealthy_threshold = "2"
}
}

resource "aws_ecs_service" "service1" {
launch_type = "FARGATE"
cluster = var.cluster_name
task_definition = var.family
name = var.service
desired_count = tonumber(var.tasks_count)

network_configuration {
security_groups = [
aws_security_group.ecs_tasks.id
]
subnets = data.aws_subnet_ids.default.ids
assign_public_ip = true
}
load_balancer {
target_group_arn = aws_lb_target_group.app.arn
container_name = "mycontainer"
container_port = 80
}
depends_on = [aws_lb_listener.https]
}
