provider "aws" {
region = "ap-south-1"
access_key = ""
secret_key = ""
}

resource "aws_ecs_cluster" "cluster" {
name = var.cluster_name
}
