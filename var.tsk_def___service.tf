variable "region" {
description = "aws_region"
type = string
default = "ap-south-1"
}

variable "cluster_name" {
description = "task_def_name"
type = string
default = "test"
}

variable "family" {
description = "task_def_name"
type = string
default = "task12"
}

variable "container_name" {
description = "container_name"
type = string
default = "mycontainer"
}

variable "image" {
description = "image_name"
type = string
default = "nginx:latest"
}

variable "cpu" {
description = "container_cpu"
type = number
default = 256
}

variable "memory" {
description = "container_memory"
type = number
default = 512
}

variable "service" {
description = ""
type = string
default = "test"
}

variable "tasks_count" {
description = "value"
type = number
default = 1
}
